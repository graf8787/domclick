CREATE TABLE IF NOT EXISTS `account` (
    `id` long PRIMARY KEY AUTO_INCREMENT NOT NULL,
    `amount` long NOT NULL
);