package ru.graf8787.other.domclick.entity

import javax.persistence.*

@Entity
data class Account (
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        var id: Long,

        @Column(nullable = false)
        var amount : Long
) {
    constructor(

    ) : this(0,0)
}