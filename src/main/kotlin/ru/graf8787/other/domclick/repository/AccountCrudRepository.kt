package ru.graf8787.other.domclick.repository

import org.springframework.data.repository.CrudRepository
import ru.graf8787.other.domclick.entity.Account

interface AccountCrudRepository : CrudRepository<Account, Long>