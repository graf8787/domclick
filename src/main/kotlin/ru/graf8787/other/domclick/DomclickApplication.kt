package ru.graf8787.other.domclick

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.orm.jpa.vendor.Database
import org.springframework.orm.jpa.vendor.HibernateJpaDialect
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.transaction.annotation.EnableTransactionManagement
import java.util.*
import javax.sql.DataSource

@SpringBootApplication
@EnableTransactionManagement
class DomclickApplication {

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(DomclickApplication::class.java, *args)
        }

    }

    @Bean
    fun dataSource(): DataSource {
        return EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("create-db.sql")
                .addScript("insert-data.sql")
                .build()
    }

    @Bean
    fun hibernateJpaVendorAdapter(): HibernateJpaVendorAdapter {
        val hibernateJpaVendorAdapter = HibernateJpaVendorAdapter()
        hibernateJpaVendorAdapter.setDatabase(Database.H2)
        return hibernateJpaVendorAdapter
    }

    @Bean
    @Autowired
    fun entityManagerFactory(
            dataSource: DataSource,
            hibernateJpaVendorAdapter: HibernateJpaVendorAdapter
    ): LocalContainerEntityManagerFactoryBean {
        val localContainerEntityManagerFactoryBean = LocalContainerEntityManagerFactoryBean()
        localContainerEntityManagerFactoryBean.dataSource = dataSource
        localContainerEntityManagerFactoryBean.jpaDialect = HibernateJpaDialect()
        localContainerEntityManagerFactoryBean.jpaVendorAdapter = hibernateJpaVendorAdapter
        localContainerEntityManagerFactoryBean.persistenceUnitName = "persistenceUnit"
        localContainerEntityManagerFactoryBean.setPackagesToScan("ru.graf8787.other.domclick.entity")
        val hibernateProperties = Properties()
        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect")
        hibernateProperties.setProperty("hibernate.show_sql", "false")
        localContainerEntityManagerFactoryBean.setJpaProperties(hibernateProperties)
        localContainerEntityManagerFactoryBean.afterPropertiesSet()
        return localContainerEntityManagerFactoryBean
    }

}