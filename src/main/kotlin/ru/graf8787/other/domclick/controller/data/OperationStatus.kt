package ru.graf8787.other.domclick.controller.data

enum class OperationStatus {
    SUCCESS,

    ACCOUNT_NOT_FOUND,

    AMOUNT_TOO_BIG,

    SAME_ACCOUNT
}