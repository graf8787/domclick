package ru.graf8787.other.domclick.controller.data

data class OperationDetails (
        val id: Long,
        val amount: Long,
        val toAccountId: Long = 0
)