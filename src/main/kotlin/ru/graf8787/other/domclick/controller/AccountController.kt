package ru.graf8787.other.domclick.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import ru.graf8787.other.domclick.controller.data.OperationDetails
import ru.graf8787.other.domclick.controller.data.OperationStatus
import ru.graf8787.other.domclick.exception.AccountNotFoundException
import ru.graf8787.other.domclick.exception.AmountTooBigException
import ru.graf8787.other.domclick.exception.SameAccountException
import ru.graf8787.other.domclick.service.IAccountService

@RestController("/account")
class AccountController( @Autowired val accountService:IAccountService ) {

    @PostMapping("/add")
    fun add(@RequestBody operationDetails: OperationDetails): OperationStatus {
        return try {
            accountService.add(operationDetails)
            OperationStatus.SUCCESS
        } catch (e: AccountNotFoundException) {
            OperationStatus.ACCOUNT_NOT_FOUND
        } catch (e: AmountTooBigException) {
            OperationStatus.AMOUNT_TOO_BIG
        } catch (e: SameAccountException) {
            OperationStatus.SAME_ACCOUNT
        }
    }

    @PostMapping("/sub")
    fun sub(@RequestBody operationDetails: OperationDetails): OperationStatus {
        return try {
            accountService.sub(operationDetails)
            OperationStatus.SUCCESS
        } catch (e: AccountNotFoundException) {
            OperationStatus.ACCOUNT_NOT_FOUND
        } catch (e: AmountTooBigException) {
            OperationStatus.AMOUNT_TOO_BIG
        } catch (e: SameAccountException) {
            OperationStatus.SAME_ACCOUNT
        }
    }

    @PostMapping("/move")
    fun move(@RequestBody operationDetails: OperationDetails): OperationStatus {
        return try {
            accountService.move(operationDetails)
            OperationStatus.SUCCESS
        } catch (e: AccountNotFoundException) {
            OperationStatus.ACCOUNT_NOT_FOUND
        } catch (e: AmountTooBigException) {
            OperationStatus.AMOUNT_TOO_BIG
        } catch (e: SameAccountException) {
            OperationStatus.SAME_ACCOUNT
        }
    }

}