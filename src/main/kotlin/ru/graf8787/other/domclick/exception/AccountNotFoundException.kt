package ru.graf8787.other.domclick.exception

class AccountNotFoundException(val id: Long) : Throwable()