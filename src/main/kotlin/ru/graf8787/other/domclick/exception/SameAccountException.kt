package ru.graf8787.other.domclick.exception

class SameAccountException(val id: Long) : Throwable()