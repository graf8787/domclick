package ru.graf8787.other.domclick.exception

class AmountTooBigException(id: Long, amount: Long) : Throwable()