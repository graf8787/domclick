package ru.graf8787.other.domclick.service

import ru.graf8787.other.domclick.controller.data.OperationDetails

interface IAccountService {

    fun add(operationDetails: OperationDetails)

    fun sub(operationDetails: OperationDetails)

    fun move(operationDetails: OperationDetails)

}
