package ru.graf8787.other.domclick.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.graf8787.other.domclick.controller.data.OperationDetails
import ru.graf8787.other.domclick.entity.Account
import ru.graf8787.other.domclick.exception.AccountNotFoundException
import ru.graf8787.other.domclick.exception.AmountTooBigException
import ru.graf8787.other.domclick.exception.SameAccountException
import ru.graf8787.other.domclick.repository.AccountCrudRepository
import javax.transaction.Transactional

@Service
class AccountService(@Autowired val accountCrudRepository: AccountCrudRepository) : IAccountService {

    @Transactional
    @Throws(AccountNotFoundException::class)
    override fun add(operationDetails: OperationDetails) {
        addOperation(operationDetails.id, operationDetails.amount)
    }

    @Transactional
    @Throws(AccountNotFoundException::class, AmountTooBigException::class)
    override fun sub(operationDetails: OperationDetails) {
        subOperation(operationDetails.id, operationDetails.amount)
    }

    @Transactional
    @Throws(AccountNotFoundException::class, AmountTooBigException::class, SameAccountException::class)
    override fun move(operationDetails: OperationDetails) {
        if (operationDetails.id == operationDetails.toAccountId)
            throw SameAccountException(operationDetails.id)
        subOperation(operationDetails.id, operationDetails.amount)
        addOperation(operationDetails.toAccountId, operationDetails.amount)
    }

    private fun addOperation(id: Long, amount: Long) {
        val account = get(id)
        account.amount += amount
        accountCrudRepository.save(account)
    }

    private fun get(id: Long): Account {
        val optional = accountCrudRepository.findById(id)
        return if (optional.isPresent) {
            optional.get()
        } else {
            throw AccountNotFoundException(id)
        }
    }

    private fun subOperation(id: Long, amount: Long) {
        val account = get(id)
        if (account.amount > amount) {
            account.amount -= amount
            accountCrudRepository.save(account)
        } else {
            throw AmountTooBigException(id, amount)
        }
    }

}