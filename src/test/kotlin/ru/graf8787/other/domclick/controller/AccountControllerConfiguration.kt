package ru.graf8787.other.domclick.controller

import org.mockito.Mockito
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile
import ru.graf8787.other.domclick.service.AccountService
import ru.graf8787.other.domclick.service.IAccountService


@Profile("testAccountController")
@Configuration
class AccountControllerConfiguration {

    @Bean
    @Primary
    fun mockAccountService(): IAccountService {
        return Mockito.mock(AccountService::class.java)
    }

}