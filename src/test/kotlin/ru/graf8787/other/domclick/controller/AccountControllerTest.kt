package ru.graf8787.other.domclick.controller

import junit.framework.TestCase.assertEquals
import junit.framework.TestCase.assertNotNull
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner
import ru.graf8787.other.domclick.DomclickApplication
import ru.graf8787.other.domclick.controller.data.OperationDetails
import ru.graf8787.other.domclick.controller.data.OperationStatus
import ru.graf8787.other.domclick.exception.AccountNotFoundException
import ru.graf8787.other.domclick.service.IAccountService

@ActiveProfiles("testAccountController")
@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(DomclickApplication::class),
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountControllerTest {

    @Autowired
    lateinit var testRestTemplate: TestRestTemplate

    @Autowired
    lateinit var accountService: IAccountService

    @Test
    fun addTest() {
        val operationDetails = OperationDetails(13, 666)
        Mockito.`when`(accountService.add(safeEq(operationDetails))).thenThrow(AccountNotFoundException::class.java)

        val result = testRestTemplate
                // ...
                .postForEntity("/add", operationDetails, OperationStatus::class.java)

        assertNotNull(result)
        assertEquals(result?.statusCode, HttpStatus.OK)
        assertEquals(result?.body, OperationStatus.ACCOUNT_NOT_FOUND)
    }

    @Test
    fun subTest() {
        val operationDetails = OperationDetails(13, 666)
        Mockito.`when`(accountService.sub(safeEq(operationDetails))).thenThrow(AccountNotFoundException::class.java)

        val result = testRestTemplate
                // ...
                .postForEntity("/sub", operationDetails, OperationStatus::class.java)

        assertNotNull(result)
        assertEquals(result?.statusCode, HttpStatus.OK)
        assertEquals(result?.body, OperationStatus.ACCOUNT_NOT_FOUND)
    }

    @Test
    fun moveTest() {
        val operationDetails = OperationDetails(13, 666, 14)
        Mockito.`when`(accountService.move(safeEq(operationDetails))).thenThrow(AccountNotFoundException::class.java)

        val result = testRestTemplate
                // ...
                .postForEntity("/move", operationDetails, OperationStatus::class.java)

        assertNotNull(result)
        assertEquals(result?.statusCode, HttpStatus.OK)
        assertEquals(result?.body, OperationStatus.ACCOUNT_NOT_FOUND)
    }

}

fun <T : Any> safeEq(value: T): T = ArgumentMatchers.eq(value) ?: value