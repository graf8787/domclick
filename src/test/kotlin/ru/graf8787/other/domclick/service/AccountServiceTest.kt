package ru.graf8787.other.domclick.service

import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import ru.graf8787.other.domclick.DomclickApplication
import ru.graf8787.other.domclick.controller.data.OperationDetails
import ru.graf8787.other.domclick.entity.Account
import ru.graf8787.other.domclick.exception.AccountNotFoundException
import ru.graf8787.other.domclick.exception.AmountTooBigException
import ru.graf8787.other.domclick.exception.SameAccountException
import ru.graf8787.other.domclick.repository.AccountCrudRepository

@RunWith(SpringRunner::class)
@SpringBootTest(classes = arrayOf(DomclickApplication::class),
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountServiceTest {

    @Autowired
    lateinit var accountService: AccountService

    @Autowired
    lateinit var accountCrudRepository: AccountCrudRepository

    @Before
    fun setUp() {
        val findAll = accountCrudRepository.findAll()
        findAll.forEach{ account: Account? -> account?.amount = 1000 }
        accountCrudRepository.saveAll(findAll)
    }

    @Test
    fun addTestSuccess() {
        val operationDetails = OperationDetails(1, 666)
        accountService.add(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 1666)
    }

    @Test
    fun subTestSuccess() {
        val operationDetails = OperationDetails(1, 666)
        accountService.sub(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 334)
    }

    @Test
    fun moveTestSuccess() {
        val operationDetails = OperationDetails(1, 666, 2)
        accountService.move(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(2).get().amount, 1666)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 334)
    }

    @Test(expected = AccountNotFoundException::class)
    fun addTestNotFound() {
        val operationDetails = OperationDetails(0, 666)
        accountService.add(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 1000)
    }

    @Test(expected = AccountNotFoundException::class)
    fun subTestNotFound() {
        val operationDetails = OperationDetails(0, 666)
        accountService.sub(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 1000)
    }

    @Test(expected = AccountNotFoundException::class)
    fun moveTestNotFound1() {
        val operationDetails = OperationDetails(0, 666, 2)
        accountService.move(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(2).get().amount, 1000)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 1000)
    }

    @Test(expected = AccountNotFoundException::class)
    fun moveTestNotFound2() {
        val operationDetails = OperationDetails(1, 666, 0)
        accountService.move(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(2).get().amount, 1000)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 1000)
    }

    @Test(expected = SameAccountException::class)
    fun moveTestSame() {
        val operationDetails = OperationDetails(1, 666, 1)
        accountService.move(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(2).get().amount, 1000)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 1000)
    }

    @Test(expected = AmountTooBigException::class)
    fun subTestTooBig() {
        val operationDetails = OperationDetails(1, 1666)
        accountService.sub(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 1000)
    }

    @Test(expected = AmountTooBigException::class)
    fun moveTestTooBig() {
        val operationDetails = OperationDetails(1, 1666, 2)
        accountService.move(operationDetails)
        Assert.assertEquals(accountCrudRepository.findById(2).get().amount, 1000)
        Assert.assertEquals(accountCrudRepository.findById(1).get().amount, 1000)
    }

}